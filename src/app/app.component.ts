import { Component } from '@angular/core';
import { Platform, ModalController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Keyboard } from "@ionic-native/keyboard";
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { StorageProvider } from '../providers/storage/storage';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = 'MainPage';

  constructor(private platform: Platform,
              private statusBar: StatusBar,
              private splashScreen: SplashScreen,
              private keyboard: Keyboard,
              private screenOrientation: ScreenOrientation,
              private storageProvider: StorageProvider,
              private modalCtrl: ModalController) {
    this.initApp();
  }

  initApp(): void {
    this.platform.ready().then(() => {
      this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      // Añade y remueve los tabs si el teclado está inactivo ó activo.
      this.keyboard.onKeyboardShow().subscribe(() => document.body.classList.add('keyboard-is-open'));
      this.keyboard.onKeyboardHide().subscribe(() => document.body.classList.remove('keyboard-is-open'));
      if (this.platform.is('android')) this.statusBar.backgroundColorByHexString('#9e0709');
      else if (this.platform.is('ios')) this.statusBar.styleDefault();
      this.validateData();
      this.splashScreen.hide();
    });
  }

  validateData() {
    this.storageProvider.emptyData()
      .then(
        empty => {
          if (empty) this.showModal('SaveQrPage');
          else this.showModal('CardPage');
        }
      )
  }

  showModal(page: string) {
    const modal = this.modalCtrl.create(page);
    modal.present();
  }

}

