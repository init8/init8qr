import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams, Platform } from 'ionic-angular';
import { File } from '@ionic-native/file';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FileOpener } from '@ionic-native/file-opener';

@IonicPage()
@Component({
  selector: 'page-qr-code',
  templateUrl: 'qr-code.html',
})
export class QrCodePage {
  contactName: string;
  codeQR: string = null;
  private FILE_NAME: string = 'contact.vcf';
  private IMAGE_NAME: string = 'image.jpeg';

  constructor(private viewCtrl: ViewController,
              private navParams: NavParams,
              private file: File,
              private fileOpener: FileOpener,
              private socialSharing: SocialSharing,
              private platform: Platform) {
    this.codeQR = this.navParams.get('data');
    this.contactName = this.navParams.get('contactName');
    this.file.writeFile(this.file.externalDataDirectory, this.FILE_NAME, this.codeQR, { replace: true });
  }

  share() {
    this.socialSharing.share('Compartir contacto', null, this.file.externalDataDirectory + this.FILE_NAME, null);
  }

  dismissModal(): void {
    this.viewCtrl.dismiss();
  }

  /** Crea una imagen del QR.  */
  getImage(): void {
    const canvas = document.querySelector('canvas') as HTMLCanvasElement; // Obtiene los datos del componente "ngx-qrcode"
    const imageData = canvas.toDataURL('image/jpeg').toString();
    fetch(imageData)
      .then(res => res.blob()) // Convierte un string BASE64 a imagen.
      .then(
        blob => {
          // Crea el archivo en el dispositivo.
          if (this.platform.is('android')) {
            this.file.writeFile(this.file.externalDataDirectory, this.IMAGE_NAME, blob, { replace: true })
              .then(
                // Abre el gestor de imagenes nativo del dispositivo para visualizar el archivo.
                () => this.fileOpener.open(this.file.externalDataDirectory + this.IMAGE_NAME, 'image/jpeg')
              )
              .catch(err => console.log(err));
          }
          else if (this.platform.is('ios')) {
            this.file.writeFile(this.file.documentsDirectory, this.IMAGE_NAME, blob, { replace: true })
              .then(() => this.fileOpener.open(this.file.documentsDirectory + this.IMAGE_NAME, 'image/jpeg'))
              .catch(err => console.log(err));
          }
        }
      )
  }

}
