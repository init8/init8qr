import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';

@IonicPage()
@Component({
  selector: 'page-card',
  templateUrl: 'card.html',
})
export class CardPage {
  data: any = {};
  qrData: string;
  imageUri: string = '';

  constructor(private viewCtrl: ViewController, 
              private storageProvider: StorageProvider) {}

  ionViewDidLoad(): void {
    this.loadCard();
  }

  dismissModal(): void {
    this.viewCtrl.dismiss();
  }

  loadCard(): void {
    this.storageProvider.getData().then(qrData => {
      if (qrData) {
        this.data = JSON.parse(qrData);
        this.qrData = this.generateQR(this.data);
      }
    });
  }

  generateQR(data: any): string {
    const N: string = ["N:" + data.name, data.surname].join(";") + "\n";
    const TEL: string = "TEL;CELL:" + data.cellphone + "\n" + "TEL;VALUE=uri;TYPE=work:tel:" + data.phone + "\n";
    const EMAIL: string = "EMAIL:" + data.email + "\n";
    const ORG: string = "ORG:" + data.org + "\n";
    const ADR: string = "ADR;TYPE=WORK:;;" + data.address + ";;;" + "\n";
    const URL: string = "URL:" + data.url + "\n";
    const vCard: string = "BEGIN:VCARD\n" + "VERSION:3.0\n" + N + EMAIL + ORG + ADR +  TEL + URL + "END:VCARD";
    return vCard;
  }

}
