import { Component } from '@angular/core';
import { IonicPage, PopoverController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-main',
  template: `
    <ion-header>
      <ion-navbar>
        <ion-title>
          <img alt="logo" class="logo" src="assets/imgs/init8.png">
        </ion-title>
        <ion-buttons right>
          <button ion-button icon-only (click)="openOptionsPopover($event)">
            <ion-icon name="more"></ion-icon>
          </button>
        </ion-buttons>
      </ion-navbar>
    </ion-header>
    <ion-content>
      <ion-tabs>
        <ion-tab [root]="tabCreateQrPage" tabTitle="Generar QR" tabIcon="barcode"></ion-tab>
        <ion-tab [root]="tabReadQrPage" tabTitle="Leer QR" tabIcon="qr-scanner"></ion-tab>
      </ion-tabs>
    </ion-content>
  `
})
export class MainPage {

  constructor(private popoverCtrl: PopoverController) { }

  tabCreateQrPage: any = 'CreateQrPage';
  tabReadQrPage: any = 'ReadQrPage';

  openOptionsPopover(event: Event): void {
    const popover = this.popoverCtrl.create('OptionsPopoverPage', {}, { cssClass: 'custom-popover' });
    popover.present({ ev: event });
  }

}
