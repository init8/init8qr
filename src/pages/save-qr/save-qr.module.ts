import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SaveQrPage } from './save-qr';

@NgModule({
  declarations: [
    SaveQrPage,
  ],
  imports: [
    IonicPageModule.forChild(SaveQrPage),
  ],
})
export class SaveQrPageModule {}
