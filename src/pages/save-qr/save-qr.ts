import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';
import { StorageProvider } from '../../providers/storage/storage';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { Base64 } from '@ionic-native/base64';
import { Keyboard } from '@ionic-native/keyboard';

@IonicPage()
@Component({
  selector: 'page-save-qr',
  templateUrl: 'save-qr.html',
})
export class SaveQrPage {
  isOpenKeyboard: boolean = false;
  name: string = '';
  surname: string = '';
  org: string = '';
  cellphone: string = '';
  phone: string = '';
  email: string = '';
  address: string = '';
  url: string = '';
  img: string = '';

  opts: ImagePickerOptions;

  constructor(private storageProvider: StorageProvider, 
              private navCtrl: NavController,
              private imagePicker: ImagePicker,
              private platform: Platform,
              private base64: Base64,
              private keyboard: Keyboard) {
    this.keyboard.onKeyboardShow().subscribe(() => this.isOpenKeyboard = true);
    this.keyboard.onKeyboardHide().subscribe(() => this.isOpenKeyboard = false);              
  }

  dismissModal(): void {
    this.platform.exitApp();
  }

  saveData(): void {
    let data = {
      'name': this.name,
      'surname': this.surname,
      'org': this.org,
      'cellphone': this.cellphone,
      'phone': this.phone,
      'email': this.email,
      'address': this.address,
      'url': this.url,
      'img': this.img
    }
    this.storageProvider.setData(data).then(() => this.navCtrl.setRoot('MainPage'));
  }

  getImage() {
    this.opts = { maximumImagesCount: 1, quality: 50 };
    this.imagePicker.getPictures(this.opts).then((results) => {
      for (let i = 0; i < results.length; i++) {
        this.base64.encodeFile(results[i]).then((base64File: string) => {
          this.img = base64File;
        });
      }
    });
  }

}
