import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';
import { AppVersion } from '@ionic-native/app-version';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
  selector: 'page-application-information',
  templateUrl: 'application-information.html',
})
export class ApplicationInformationPage {
  appName: string;
  versionNumber: string;
  private URL_LICENSE: string = 'https://www.apache.org/licenses/LICENSE-2.0';
  urls = [
    { name: 'Facebook', icon: 'logo-facebook', color: 'facebook-color', url: 'https://www.facebook.com/init8' },
    { name: 'Twitter', icon: 'logo-twitter', color: 'twitter-color', url: 'https://twitter.com/init8Oficial' },
    { name: 'LinkedIn', icon: 'logo-linkedin', color: 'linkedin-color', url: 'https://www.linkedin.com/company/init8-oficial/' },
    { name: 'Web Site', icon: 'globe', color: 'primary', url: 'http://www.init8.com' }
  ];
  repositories = [
    { name: 'Bitbucket', url: 'https://bitbucket.org/init8/init8QR' },
    { name: 'Github', url: 'https://github.com/init8oficial/init8QR' }
  ];
  qrGenerators = [
    { url: 'https://www.the-qrcode-generator.com/' },
    { url: 'https://es.qr-code-generator.com/' }
  ]

  constructor(private viewCtrl: ViewController,
              private appVersion: AppVersion,
              private inAppBrowser: InAppBrowser) { }

  ionViewWillEnter(): void {
    this.getAppName();
    this.getVersionNumber();
  }

  getAppName(): void {
    this.appVersion.getAppName().then(appName => this.appName = appName);
  }

  getVersionNumber(): void {
    this.appVersion.getVersionNumber().then(versionNumber => this.versionNumber = versionNumber);
  }

  /** Abre la licencia en el navegador.  */
  openLicense(): void {
    this.inAppBrowser.create(this.URL_LICENSE);
  }

  openUrl(url: string): void {
    this.inAppBrowser.create(url);
  }

  dismissModal(): void {
    this.viewCtrl.dismiss();
  }

}
