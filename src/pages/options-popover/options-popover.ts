import { Component } from '@angular/core';
import { IonicPage, ViewController, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-options-popover',
  template: `
    <ion-list no-lines no-margin>
      <button ion-item (click)="showModal('CardPage')">
        Código QR
      </button>
      <button ion-item (click)="showModal('ApplicationInformationPage')">
        Info. de la aplicación
      </button>
      <!--
      <button ion-item (click)="showModal('EditQrPage')">
        Editar código QR
      </button>
      -->
    </ion-list>
  `
})
export class OptionsPopoverPage {

  constructor(private viewCtrl: ViewController, 
              private modalCtrl: ModalController) { }

  showModal(page: string): void {
    const modal = this.modalCtrl.create(page);
    modal.present();
    this.viewCtrl.dismiss();
  }

}
