import { Component } from '@angular/core';
import { IonicPage, ToastController, Platform } from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { Keyboard } from '@ionic-native/keyboard';

@IonicPage()
@Component({
  selector: 'page-read-qr',
  templateUrl: 'read-qr.html',
})
export class ReadQrPage {
  private FILE_NAME: string = 'contact.vcf';
  isOpenKeyboard: boolean = false;

  constructor(private toastCtrl: ToastController,
              private barcodeScanner: BarcodeScanner,
              private file: File,
              private fileOpener: FileOpener,
              private keyboard: Keyboard,
              private platform: Platform) {
    this.keyboard.onKeyboardShow().subscribe(() => this.isOpenKeyboard = true);
    this.keyboard.onKeyboardHide().subscribe(() => this.isOpenKeyboard = false);
  }

  /** Lee el código QR en formato VCARD e importa el contacto.  */
  getContactFromQR(): void {
    this.barcodeScanner.scan({ disableSuccessBeep: true, resultDisplayDuration: 0, orientation: 'portrait' })
      .then(data => {
        if (data.text == '') this.toastMessage('Error al obtener la información');
        else {
          if (data.text.includes('BEGIN:VCARD')) this.importContact(data.text);
          else this.toastMessage('Contacto inválido');
        }
      })
      .catch(e => alert(JSON.stringify(e)));
  }

  /**
     * Importa el contacto en el dispositivo.
     * @param {vCard} string Formato VCARD.
  */
  importContact(vCard: string): void {
    // Crea el contacto en el dispositivo.
    if (this.platform.is('android')) {
      this.file.writeFile(this.file.externalDataDirectory, this.FILE_NAME, vCard, { replace: true })
        .then(
          // Abre la aplicación nativa del dispositivo para importar contactos.
          () => this.fileOpener.open(this.file.externalDataDirectory + this.FILE_NAME, 'text/x-vcard')
        )
        .catch(e => alert(JSON.stringify(e)));
    }
    else if (this.platform.is('ios')) {
      this.file.writeFile(this.file.documentsDirectory, this.FILE_NAME, vCard, { replace: true })
        .then(() => this.fileOpener.open(this.file.documentsDirectory + this.FILE_NAME, 'text/x-vcard'))
        .catch(e => alert(JSON.stringify(e))); 
    }
  }

  toastMessage(msg: string): void {
    const toast = this.toastCtrl.create({
      message: msg,
      showCloseButton: true,
      closeButtonText: 'Ok',
      cssClass: 'toast-tabs'
    });
    toast.present();
  }

}
