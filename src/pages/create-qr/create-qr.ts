import { Component } from '@angular/core';
import { IonicPage, AlertController, ModalController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { Contacts, Contact, IContactError } from '@ionic-native/contacts';

@IonicPage()
@Component({
  selector: 'page-create-qr',
  templateUrl: 'create-qr.html',
})
export class CreateQrPage {
  isOpenKeyboard: boolean = false;
  name: string = '';
  surname: string = '';
  org: string = '';
  cellphone: string = '';
  phone: string = '';
  email: string = '';
  address: string = '';
  url: string = '';

  constructor(private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private keyboard: Keyboard,
              private contacts: Contacts) {
    this.keyboard.onKeyboardShow().subscribe(() => this.isOpenKeyboard = true);
    this.keyboard.onKeyboardHide().subscribe(() => this.isOpenKeyboard = false);
  }

  /** Genera el código QR con el formato de VCARD - v3.0.  */
  openQrCodePage() {
    const N: string = ["N:" + this.name, this.surname].join(";") + "\n";
    const TEL: string = "TEL;CELL:" + this.cellphone + "\n" + "TEL;VALUE=uri;TYPE=work:tel:" + this.phone + "\n";
    const EMAIL: string = "EMAIL:" + this.email + "\n";
    const ORG: string = "ORG:" + this.org + "\n";
    const ADR: string = "ADR;TYPE=WORK:;;" + this.address + ";;;" + "\n";
    const URL: string = "URL:" + this.url + "\n";
    const vCard: string = "BEGIN:VCARD\n" + "VERSION:3.0\n" + N + EMAIL + ORG + ADR +  TEL + URL + "END:VCARD";
    const confirm = this.alertCtrl.create({
      subTitle: 'Generar código QR',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Confirmar',
          handler: () => {
            const modal = this.modalCtrl.create('QrCodePage', { data: vCard, contactName: [this.name, this.surname].join(" ") });
            modal.present();
          }
        }
      ]
    });
    confirm.present();
  }

  showContacts(): void {
    const modal = this.modalCtrl.create('ContactsPage');
    modal.present();
  }

  /** Selecciona un contacto y regresa los datos.  */
  // TODO: Verificar campos de teléfono
  pickContact(): void {
    this.resetVariables();
    this.contacts.pickContact()
      .then((contact: Contact) => {
        this.name = contact.name.familyName;
        this.surname = contact.name.givenName;
        if (contact.organizations) this.org = contact.organizations[0].name;
        if (contact.phoneNumbers) {
          for (let phoneNumber of contact.phoneNumbers) {
            if (phoneNumber.type === 'mobile') this.cellphone = phoneNumber.value;
            else if (phoneNumber.type === 'work') this.phone = phoneNumber.value;
          }
        }
        if (contact.emails) this.email = contact.emails[0].value;
        if (contact.addresses) this.address = contact.addresses[0].formatted;
        if (contact.urls) this.url = contact.urls[0].value;
      })
      .catch((e: IContactError) => console.log(e))
  }

  resetVariables(): void {
    this.name = '';
    this.surname = '';
    this.org = '';
    this.cellphone = '';
    this.phone = '';
    this.email = '';
    this.address = '';
    this.url = '';
  }

}
