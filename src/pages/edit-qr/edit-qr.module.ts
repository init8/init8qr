import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditQrPage } from './edit-qr';

@NgModule({
  declarations: [
    EditQrPage,
  ],
  imports: [
    IonicPageModule.forChild(EditQrPage),
  ],
})
export class EditQrPageModule {}
