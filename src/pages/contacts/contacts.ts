import { Component } from '@angular/core';
import { IonicPage, ModalController, AlertController, ViewController, Platform } from 'ionic-angular';
import { Contacts, Contact } from '@ionic-native/contacts';

@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {
  searchTerm: string = '';
  contactsList: Contact[] = [];
  contactsFilter = [];

  constructor(private contacts: Contacts,
              private modalCtrl: ModalController,
              private alertCtrl: AlertController,
              private viewCtrl: ViewController,
              private platform: Platform) { }

  ionViewWillEnter(): void {
    this.getContactsList();
  }

  /** Obtiene todos los contactos del dispositivo.  */
  getContactsList(): void {
    this.contacts.find(['*'])
      .then(
        contacts => {
          this.contactsList = contacts;
          this.contactsFilter = this.contactsList;
        }
      );
  }

  /**
     * Genera el formato VCARD - v3.0., más info: https://tools.ietf.org/html/rfc6350#section-6.4.1
     * @param {Contact} fields Campos de tipo contacto del plugin Contacts en Ionic Native.
  */
  openQrCodePage(contact: Contact) {
    const data: Contact = contact['_objectInstance']; // Obtiene los datos de la instancia de tipo contact.
    const name: string = data.name.givenName;
    let surname: string = '';
    let tels: string = '';
    let email: string = '';
    let org: string = '';
    let adr: string = '';
    let url: string = '';
    // Comprueba que los campos no sean nulos.
    if (data.name.familyName) {
      surname = data.name.familyName;
    }
    if (data.phoneNumbers) {
      for (let phoneNumber of data.phoneNumbers) {
        // Formato para generar el campo TEL de una VCARD es importante que termine con un salto de línea, 
        // ejemplo: TEL;VALUE=uri;TYPE=home:tel:9991054556
        // Este formato es similiar en todos los campos.
        tels += "TEL;VALUE=uri;TYPE=" + phoneNumber.type + ":" + phoneNumber.value + "\n";
      }
    }
    if (data.emails) {
      email += "EMAIL:" + data.emails[0].value + "\n";
    }
    if (data.organizations) {
      org += "ORG:" + data.organizations[0].name + "\n";
    }
    if (data.addresses) {
      adr += "ADR;TYPE=" + data.addresses[0].type + ",PREF:;;" +
        [data.addresses[0].streetAddress, data.addresses[0].locality, data.addresses[0].region, data.addresses[0].postalCode + "\n"]
          .join(";");
    }
    if (data.urls) {
      url += "URL:" + data.urls[0].value + "\n";
    }
    const N: string = ["N:" + name, surname].join(";") + "\n";
    // Formato VCARD - v3.0. Consulta los ejemplos en el link de arriba.
    const vCard: string = "BEGIN:VCARD\n" + "VERSION:3.0\n" + N + tels + email + org + adr + url + "END:VCARD";
    const confirm = this.alertCtrl.create({
      subTitle: 'Generar código QR',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Confirmar',
          handler: () => {
            const modal = this.modalCtrl.create('QrCodePage', { data: vCard, contactName: contact.displayName });
            modal.present();
          }
        }
      ]
    });
    confirm.present();
  }

  /** Filtra un contacto.  */
  setFilter() {
      this.contactsFilter = this.contactsList
      .filter(
        contactFilter => {
          if (this.platform.is('android')) return contactFilter.displayName.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
          else if (this.platform.is('ios')) return contactFilter.name.formatted.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
        }
      )
  }

  dismissModal(): void {
    this.viewCtrl.dismiss();
  }

}
