import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class StorageProvider {
  private KEY_NAME: string = 'qrData';

  constructor(private storage: Storage) { }

  async setData(data: any): Promise<any> {
    return this.storage.set(this.KEY_NAME, JSON.stringify(data));
  }

  async getData(): Promise<any> {
    return this.storage.get(this.KEY_NAME);
  }

  async emptyData(): Promise<boolean> {
    return this.getData()
      .then(
        data => {
          if (data) return false;
          else return true;
        }
      )
  }

}
