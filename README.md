# INIT8QR

Para un correcto funcionamiento del proyecto se requiere de Cordova e Ionic, es necesario tener las siguientes consideraciones en cuanto al entorno de trabajo y dependencias.

## Requerimientos
* Java de Oracle version 1.8
* Android Sdk (Se descarga de la pagina oficial de Android).
* Node.js versión 10.14.1
* Cordova versión 8.1.2
* Ionic versión 4.5.0

## Inicializar el proyecto

Clonamos el proyecto desde el repositorio
```
$ git clone https://<username>@bitbucket.org/init8/init8qr.git
```

Instalamos los paquetes nativos o plugins de cordova.
```
$ ionic cordova prepare
```

Instalamos los paquetes para node.js
```
$ npm install
```

## Build and Run

Para compilar el proyecto se ejecuta el siguiente comando.
```
$ ionic cordova build android
```

Para correr el proyecto en del dispositivo se ejecuta el siguiente comando.
```
$ ionic cordova run android
```

